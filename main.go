package main

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
)

type (
	Post struct {
		ID       int    `json:"id"`
		Activity string `json:"activity"`
	}
)

var (
	db *sql.DB
)

var posts []Post

func getActivity(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	id, _ := strconv.Atoi(params["id"])

	for _, item := range posts {
		if item.ID == id {
			json.NewEncoder(w).Encode(item)
			break
		}
		return
	}
	json.NewEncoder(w).Encode(&Post{})
}

func getActivities(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var result []Post

	result = make([]Post, 0)

	query := `select id, activities from to_do_list`
	rows, _ := db.Query(query)

	defer rows.Close()

	for rows.Next() {
		post := Post{}

		rows.Scan(&post.ID, &post.Activity)
		result = append(result, post)
	}

	json.NewEncoder(w).Encode(result)

}

func postActivity(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	reqBody := json.NewDecoder(r.Body)
	var s Post
	reqBody.Decode(&s)

	query := `INSERT INTO to_do_list (id, activities) VALUES ($1, $2)`

	db.Query(query, s.ID, s.Activity)

	json.NewEncoder(w).Encode(s)
}

func main() {

	connStr := "postgres://postgres:postgres@localhost:5432/probation_golang?sslmode=disable"
	dbInstance, err := sql.Open("postgres", connStr)

	if err != nil {
		log.Fatal(err)
	}

	db = dbInstance

	defer db.Close()

	router := mux.NewRouter()

	posts = append(posts, Post{ID: 1, Activity: "Makan"})
	posts = append(posts, Post{ID: 2, Activity: "Mandi"})

	router.HandleFunc("/activities", getActivities).Methods("GET")
	router.HandleFunc("/activity/{id}", getActivity).Methods("GET")
	router.HandleFunc("/postactivity", postActivity).Methods("POST")

	http.ListenAndServe(":8080", router)
}
